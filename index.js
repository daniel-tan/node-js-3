const express = require("express");
const app = express();
const port = 3000;
const fs = require("fs");
const morgan = require("morgan");
app.set("view engine", "ejs");
app.use(express.json());
app.use(express.urlencoded({ extended: true })); 
app.use(morgan("dev"));


app.get("/", (req, res) => {
     fs.readFile("games.json", (err, data) => {
          res.render("index.ejs", {
               games: JSON.parse(data),
          });
     });
});

app.post("/add", (req, res) => {
     const nama = req.body.nama;
     const genre = req.body.genre;
     fs.readFile("games.json", "utf8", (err, data) => {
          const getGames = JSON.parse(data);
          getGames.push({
               id: getGames[getGames.length - 1].id + 1,
               name: nama,
               genre: genre,
          });
          fs.writeFile("games.json", JSON.stringify(getGames), err => {
               console.log("data baru : ", getGames);
               res.redirect('/');    
          });
     });
});

app.get('/delete', (req, res) => {
     const gameID = JSON.parse(req.query.id);

     fs.readFile("games.json", "utf8", (err, data) => {
          const getGames = JSON.parse(data);
          const gamesFiltered = getGames.filter(games => {
               return games.id !== gameID;
          });
          fs.writeFile("games.json", JSON.stringify(gamesFiltered), err => {
               res.redirect('/');
          });
     });
});

app.get('/update', (req, res) => {
     const gameID = JSON.parse(req.query.id);

     fs.readFile("games.json", "utf8", (err, data) => {
          const getGames = JSON.parse(data);
          const gamesFiltered = getGames.filter(games => {
               return games.id == gameID;
          });
          // console.log("cek data = ", gamesFiltered[0].name);
          res.render("update.ejs", {
               games: gamesFiltered,
          });
     });
});

app.post("/update", (req, res) => {
     const gameID = req.body.id;
     const gameName = req.body.nama;
     const gameGenre = req.body.genre;
     console.log("cek data masuk : ", gameID, gameName, gameGenre);

     fs.readFile("games.json", "utf8", (err, data) => {
          const getGames = JSON.parse(data);
          getGames.forEach(games => {
               if (games.id == gameID) {
                    games.name = gameName;
                    games.genre = gameGenre;
               }
          });
          fs.writeFile("games.json", JSON.stringify(getGames), err => {
               console.log("test ubah : ", getGames);
               res.redirect("/");
          });
     });
});

app.listen(port, () => { 
     console.log(`Example app listening on port ${port}`);
     console.log("Press Ctrl+C to quit.");
});  